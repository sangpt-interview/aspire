### Run application in one go
- go to working directory
- make sure the host machine is free at port 80 (http) and 3306 (mysql). If not, please set these ports in `.env`.
  + `DOCKER_NGINX_EXTERNAL_PORT=80` 
  + `DOCKER_MYSQL_EXTERNAL_PORT=3306`
- run `./start.sh`
- server should start at location: `http://localhost`

### Brief Documentation

- `Foundation` - where provides common services.
- `Admin Portal` - used by administrations
  - They can login / logout the system
  - See all loans
  - View loan in detail, they can see repayment history of this loan.
  - If the loans are in status `new`, administrations can approve them.
- `User Portal` - used by end users
  - They can login / logout the system.
  - Can only manage there owned loans.
    + See list
    + View loan in detail, they can see repayment history of this loan.
    + Create new loan
    + When loans have not `approved` yet (still be in status `new`), end users can update the loans.
  - They can repay the loan
    + Can only repay if loans are in status `approved`
    + When paying of the loan, loan will be automatically updated to `done` 

### Authentication - check postman collection in folder `docs`
- `Admin Portal` - check guard `apiAdmin`
  - Run api login:
    + URL: `{{URL}}/api/v1/admin/login`
    + Method: POST
    + payload: `{"email": "admin@aspire.test.com","password": "123456"}`
    + Should see to token, then copy it and assign to environment variable `ADMIN-TOKEN` in format: `Bearer {token}`
- `User Portal` - check guard `apiUser`
  - Run api login:
    + URL: `{{URL}}/api/v1/login`
    + Method: POST
    + payload: `{"email": "user@aspire.test.com","password": "123456"}`
    + Should see to token, then copy it and assign to environment variable `USER-TOKEN` in format: `Bearer {token}`

### All Apis are provided under postman collection in folder `docs`

### Run test
- Run `docker-compose exec app vendor/bin/phpunit`