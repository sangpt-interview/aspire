<?php

return [
    'success' => 'Success.',
    'error' => 'Error.',
    'invalid_request' => 'The given data was invalid.',
    'unauthorized' => 'Unauthorized.',
    'login_failed' => 'These credentials do not match our records.',
    'not_found' => 'Not found.',
];
