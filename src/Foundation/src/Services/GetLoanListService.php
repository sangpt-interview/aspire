<?php
declare(strict_types=1);

namespace Aspire\Foundation\Services;


use Aspire\Foundation\Repositories\Contracts\LoanRepository;
use Aspire\Foundation\Services\Requests\GetLoanListRequest;

/**
 * Class GetLoanListService
 * @package Aspire\Foundation\Services
 */
class GetLoanListService
{
    /**
     * @var LoanRepository
     */
    protected LoanRepository $loanRepository;

    /**
     * GetLoanService constructor.
     * @param LoanRepository $loanRepository
     */
    public function __construct(LoanRepository $loanRepository)
    {
        $this->loanRepository = $loanRepository;
    }

    /**
     * @param GetLoanListRequest|null $request
     * @return array
     */
    public function execute(?GetLoanListRequest $request = null): array
    {
        return $this->loanRepository->get($request);
    }
}