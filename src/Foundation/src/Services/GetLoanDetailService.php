<?php
declare(strict_types=1);

namespace Aspire\Foundation\Services;


use Aspire\Foundation\Repositories\Contracts\LoanRepository;

/**
 * Class GetLoanDetailService
 * @package Aspire\Foundation\Services
 */
class GetLoanDetailService
{
    /**
     * @var LoanRepository
     */
    protected LoanRepository $loanRepository;

    /**
     * GetLoanService constructor.
     * @param LoanRepository $loanRepository
     */
    public function __construct(LoanRepository $loanRepository)
    {
        $this->loanRepository = $loanRepository;
    }

    /**
     * @param int $loanId
     * @return array
     */
    public function execute(int $loanId): array
    {
        return $this->loanRepository->getDetail($loanId);
    }
}