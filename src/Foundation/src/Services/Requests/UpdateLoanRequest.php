<?php
declare(strict_types=1);

namespace Aspire\Foundation\Services\Requests;


use Aspire\Foundation\Models\Loan;
use Carbon\Carbon;

/**
 * Class UpdateLoanRequest
 * @package Aspire\Foundation\Services\Requests
 */
class UpdateLoanRequest
{
    /**
     * @var Loan
     */
    protected Loan $loan;
    /**
     * @var int
     */
    protected int $userId;
    /**
     * @var float
     */
    protected float $amountRequired;
    /**
     * @var Carbon
     */
    protected Carbon $loanTerm;

    /**
     * UpdateLoanRequest constructor.
     * @param int $userId
     * @param Loan $loan
     * @param float $amountRequired
     * @param Carbon $loanTerm
     */
    public function __construct(Loan $loan, int $userId, float $amountRequired, Carbon $loanTerm)
    {
        $this->loan = $loan;
        $this->userId = $userId;
        $this->amountRequired = $amountRequired;
        $this->loanTerm = $loanTerm;
    }

    /**
     * @return Loan
     */
    public function getLoan(): Loan
    {
        return $this->loan;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @return float
     */
    public function getAmountRequired(): float
    {
        return $this->amountRequired;
    }

    /**
     * @return Carbon
     */
    public function getLoanTerm(): Carbon
    {
        return $this->loanTerm;
    }
}