<?php
declare(strict_types=1);

namespace Aspire\Foundation\Services\Requests;


use Carbon\Carbon;

/**
 * Class CreateLoanRequest
 * @package Aspire\Foundation\Services\Requests
 */
class CreateLoanRequest
{
    /**
     * @var int
     */
    protected int $userId;
    /**
     * @var float
     */
    protected float $amountRequired;
    /**
     * @var Carbon
     */
    protected Carbon $loanTerm;

    /**
     * GetLoanRequest constructor.
     * @param int $userId
     * @param float $amountRequired
     * @param Carbon $loanTerm
     */
    public function __construct(int $userId, float $amountRequired, Carbon $loanTerm)
    {
        $this->userId = $userId;
        $this->amountRequired = $amountRequired;
        $this->loanTerm = $loanTerm;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @return float
     */
    public function getAmountRequired(): float
    {
        return $this->amountRequired;
    }

    /**
     * @return Carbon
     */
    public function getLoanTerm(): Carbon
    {
        return $this->loanTerm;
    }
}