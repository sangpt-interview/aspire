<?php
declare(strict_types=1);

namespace Aspire\Foundation\Services\Requests;


/**
 * Class GetLoanListRequest
 * @package Aspire\Foundation\Services\Requests
 */
class GetLoanListRequest
{
    /**
     * @var int|null
     */
    protected ?int $userId;

    /**
     * GetLoanRequest constructor.
     * @param int|null $userId
     */
    public function __construct(?int $userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }
}