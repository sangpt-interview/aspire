<?php
declare(strict_types=1);

namespace Aspire\Foundation\Services;


use Aspire\Foundation\Repositories\Contracts\LoanRepository;
use Aspire\Foundation\Services\Requests\UpdateLoanRequest;

/**
 * Class UpdateLoanService
 * @package Aspire\Foundation\Services
 */
class UpdateLoanService
{
    /**
     * @var LoanRepository
     */
    protected LoanRepository $loanRepository;

    /**
     * UpdateLoanService constructor.
     * @param LoanRepository $loanRepository
     */
    public function __construct(LoanRepository $loanRepository)
    {
        $this->loanRepository = $loanRepository;
    }

    /**
     * @param UpdateLoanRequest $updateLoanRequest
     */
    public function execute(UpdateLoanRequest $updateLoanRequest)
    {
        $this->loanRepository->update($updateLoanRequest);
    }
}