<?php
declare(strict_types=1);

namespace Aspire\Foundation\Services;


use Aspire\Foundation\Repositories\Contracts\LoanRepository;
use Aspire\Foundation\Services\Requests\CreateLoanRequest;

/**
 * Class CreateLoanService
 * @package Aspire\Foundation\Services
 */
class CreateLoanService
{
    /**
     * @var LoanRepository
     */
    protected LoanRepository $loanRepository;

    /**
     * CreateLoanService constructor.
     * @param LoanRepository $loanRepository
     */
    public function __construct(LoanRepository $loanRepository)
    {
        $this->loanRepository = $loanRepository;
    }

    /**
     * @param CreateLoanRequest $createLoanRequest
     */
    public function execute(CreateLoanRequest $createLoanRequest)
    {
        $this->loanRepository->create($createLoanRequest);
    }
}