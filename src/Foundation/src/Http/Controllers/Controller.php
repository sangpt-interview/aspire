<?php
declare(strict_types=1);

namespace Aspire\Foundation\Http\Controllers;


use App\Http\Controllers\Controller as LaravelController;
use Aspire\Foundation\Helpers\HasResponse;

/**
 * Class Controller
 * @package Aspire\Foundation\Http\Controllers
 */
class Controller extends LaravelController
{
    use HasResponse;
}