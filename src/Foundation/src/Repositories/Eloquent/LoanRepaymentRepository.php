<?php
declare(strict_types=1);

namespace Aspire\Foundation\Repositories\Eloquent;


use Aspire\Foundation\Models\LoanRepayment;
use Aspire\Foundation\Repositories\Contracts\LoanRepaymentRepository as LoanRepaymentRepositoryContract;

/**
 * Class LoanRepaymentRepository
 * @package Aspire\Foundation\Repositories\Eloquent
 */
class LoanRepaymentRepository implements LoanRepaymentRepositoryContract
{
    /**
     * @inheritDoc
     */
    public function create(array $data)
    {
        LoanRepayment::query()->create($data);
    }
}