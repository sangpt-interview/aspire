<?php
declare(strict_types=1);

namespace Aspire\Foundation\Repositories\Eloquent;


use Aspire\Foundation\Enums\LoanStatusEnum;
use Aspire\Foundation\Models\Loan;
use Aspire\Foundation\Models\LoanRepayment;
use Aspire\Foundation\Repositories\Contracts\LoanRepository as LoanRepositoryContract;
use Aspire\Foundation\Services\Requests\CreateLoanRequest;
use Aspire\Foundation\Services\Requests\GetLoanListRequest;
use Aspire\Foundation\Services\Requests\UpdateLoanRequest;
use Illuminate\Support\Facades\DB;

/**
 * Class LoanRepository
 * @package Aspire\Foundation\Repositories\Eloquent
 */
class LoanRepository implements LoanRepositoryContract
{
    /**
     * @inheritDoc
     */
    public function get(?GetLoanListRequest $request): array
    {
        $query = Loan::query();

        if ($request && $request->getUserId()) {
            $query->where('user_id', '=', $request->getUserId());
        }

        return $query
            ->get()
            ->map(function (Loan $loan) {
                return [
                    'id' => (int) $loan->id,
                    'user_id' => (int) $loan->user_id,
                    'amount_required' => (float) $loan->amount_required,
                    'remaining_amount' => (float) $loan->remaining_amount,
                    'loan_term' => $loan->loan_term->format('Y-m-d'),
                    'status' => (string) $loan->status,
                    'approved_by' => (int) $loan->approved_by,
                    'created_at' => $loan->created_at->format('Y-m-d'),
                ];
            })
            ->toArray();
    }

    /**
     * @inheritDoc
     */
    public function getDetail(int $loanId): array
    {
        $loan = Loan::query()->with(['repayments'])->find($loanId);

        if (! $loan) {
            return [];
        }

        if ($loan->repayments) {
            $repayments = $loan->repayments->map(function (LoanRepayment $loanRepayment) {
                return [
                    'amount_repaid' => (float) $loanRepayment->amount_repaid,
                    'created_at' => $loanRepayment->created_at->format('Y-m-d H:i:s'),
                ];
            });
        } else {
            $repayments = [];
        }

        return [
            'id' => (int) $loan->id,
            'user_id' => (int) $loan->user_id,
            'amount_required' => (float) $loan->amount_required,
            'remaining_amount' => (float) $loan->remaining_amount,
            'loan_term' => $loan->loan_term->format('Y-m-d'),
            'status' => (string) $loan->status,
            'approved_by' => (int) $loan->approved_by,
            'created_at' => $loan->created_at->format('Y-m-d'),
            'repayments' => $repayments
        ];
    }

    /**
     * @inheritDoc
     */
    public function create(CreateLoanRequest $request)
    {
        Loan::query()
            ->create([
                'user_id' => $request->getUserId(),
                'amount_required' => $request->getAmountRequired(),
                'remaining_amount' => $request->getAmountRequired(),
                'loan_term' => $request->getLoanTerm(),
            ]);
    }

    /**
     * @inheritDoc
     */
    public function update(UpdateLoanRequest $request)
    {
        Loan::query()
            ->whereKey($request->getLoan()->id)
            ->update([
                'amount_required' => $request->getAmountRequired(),
                'remaining_amount' => $request->getAmountRequired(),
                'loan_term' => $request->getLoanTerm(),
            ]);
    }

    /**
     * @inheritDoc
     */
    public function approve(int $loanId, int $approvedBy)
    {
        Loan::query()
            ->whereKey($loanId)
            ->update([
                'status' => LoanStatusEnum::APPROVED
            ]);
    }

    /**
     * @inheritDoc
     */
    public function repaid(int $loanId, float $amountRepaid)
    {
        Loan::query()
            ->whereKey($loanId)
            ->update([
                'remaining_amount' => DB::raw("remaining_amount - {$amountRepaid}")
            ]);
    }

    /**
     * @inheritDoc
     */
    public function finishLoan(int $loanId)
    {
        Loan::query()
            ->whereKey($loanId)
            ->update([
                'status' => LoanStatusEnum::DONE
            ]);
    }
}