<?php
declare(strict_types=1);

namespace Aspire\Foundation\Repositories\Contracts;


/**
 * Interface LoanRepaymentRepository
 * @package Aspire\Foundation\Repositories\Contracts
 */
interface LoanRepaymentRepository
{
    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data);
}