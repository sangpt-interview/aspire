<?php
declare(strict_types=1);

namespace Aspire\Foundation\Repositories\Contracts;


use Aspire\Foundation\Services\Requests\CreateLoanRequest;
use Aspire\Foundation\Services\Requests\GetLoanListRequest;
use Aspire\Foundation\Services\Requests\UpdateLoanRequest;

/**
 * Interface LoanRepository
 * @package Aspire\Foundation\Repositories\Contracts
 */
interface LoanRepository
{
    /**
     * @param GetLoanListRequest|null $request
     * @return array
     */
    public function get(?GetLoanListRequest $request): array;

    /**
     * @param int $loanId
     * @return array
     */
    public function getDetail(int $loanId): array;

    /**
     * @param CreateLoanRequest $request
     */
    public function create(CreateLoanRequest $request);

    /**
     * @param UpdateLoanRequest $request
     */
    public function update(UpdateLoanRequest $request);

    /**
     * @param int $loanId
     * @param int $approvedBy
     */
    public function approve(int $loanId, int $approvedBy);

    /**
     * @param int $loanId
     * @param float $amountRepaid
     * @return mixed
     */
    public function repaid(int $loanId, float $amountRepaid);

    /**
     * @param int $loanId
     * @return mixed
     */
    public function finishLoan(int $loanId);
}