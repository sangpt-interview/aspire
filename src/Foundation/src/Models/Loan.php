<?php
declare(strict_types=1);

namespace Aspire\Foundation\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Loan
 * @package Aspire\Foundation\Models
 */
class Loan extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'amount_required',
        'remaining_amount',
        'loan_term',
        'status',
        'approved_by',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'loan_term',
    ];

    /**
     * @return HasMany
     */
    public function repayments(): HasMany
    {
        return $this->hasMany(LoanRepayment::class);
    }
}
