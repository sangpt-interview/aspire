<?php
declare(strict_types=1);

namespace Aspire\Foundation\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class LoanRepayment
 * @package Aspire\Foundation\Models
 */
class LoanRepayment extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'loan_id',
        'amount_repaid'
    ];
}
