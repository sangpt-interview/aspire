<?php
declare(strict_types=1);

namespace Aspire\Foundation\Helpers;


use Illuminate\Http\JsonResponse;

/**
 * Trait HasResponse
 * @package App\Helpers
 */
trait HasResponse
{
    /**
     * @param string $message
     * @param array $data
     * @param array $extend
     * @return JsonResponse
     */
    public function success(string $message, array $data = [], array $extend = []): JsonResponse
    {
        $response = [
            'success' => true,
            'message' => $message
        ];

        if ($data) {
            $response['data'] = $data;
        }

        if ($extend) {
            $response = array_merge($response, $extend);
        }

        return response()->json($response);
    }

    /**
     * @param string $message
     * @param int $code
     * @param array $errors
     * @return JsonResponse
     */
    public function error(string $message, int $code = 500, array $errors = []): JsonResponse
    {
        $response = [
            'success' => false,
            'message' => $message
        ];

        if (!empty($errors)) {
            $response['errors'] = $errors;
        }

        if ($code === 0 || $code > 500) {
            $code = 500;
        }

        return response()->json($response, $code);
    }
}
