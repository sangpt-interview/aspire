<?php
declare(strict_types=1);

namespace Aspire\Foundation\Exceptions;


use App\Exceptions\Handler as ExceptionHandler;
use Aspire\Foundation\Helpers\HasResponse;
use Illuminate\Http\Response;
use Throwable;

/**
 * Class Handler
 * @package Aspire\Foundation\Exceptions
 */
class Handler extends ExceptionHandler
{
    use HasResponse;

    /**
     * @inheritDoc
     */
    public function render($request, Throwable $e)
    {
        switch (get_class($e)) {
            case 'Illuminate\Validation\ValidationException':
                return $this->error(
                    __('AspireFoundation::messages.invalid_request'),
                    Response::HTTP_BAD_REQUEST,
                    $e->errors()
                );

            case 'Illuminate\Auth\AuthenticationException':
                return $this->error(
                    __('AspireFoundation::messages.unauthorized'),
                    Response::HTTP_UNAUTHORIZED
                );

            case 'Illuminate\Database\Eloquent\ModelNotFoundException':
                return $this->error(
                    __('AspireFoundation::messages.not_found'),
                    Response::HTTP_NOT_FOUND
                );
            default :

                if (env('APP_DEBUG')) {
                    $errorMessage = $e->getMessage();
                } else {
                    $errorMessage = __('AspireFoundation::messages.error');
                }

                return $this->error($errorMessage, (int) $e->getCode());
        }
    }
}
