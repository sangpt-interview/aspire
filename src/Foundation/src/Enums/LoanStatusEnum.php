<?php
declare(strict_types=1);

namespace Aspire\Foundation\Enums;


/**
 * Class LoanStatusEnum
 * @package Aspire\Foundation\Enums
 */
class LoanStatusEnum
{
    public const NEW = 'new';
    public const APPROVED = 'approved';
    public const DONE = 'done';

    /**
     * @return string[]
     */
    public static function all(): array
    {
        return [self::NEW, self::APPROVED, self::DONE];
    }
}
