<?php
declare(strict_types=1);

namespace Aspire\Foundation;


use Aspire\Foundation\Exceptions\Handler;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Support\ServiceProvider;

/**
 * Class FoundationServiceProvider
 * @package Aspire\Foundation
 */
class FoundationServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(ExceptionHandler::class, Handler::class);
        $this->app->bind('Aspire\\Foundation\\Repositories\\Contracts\\LoanRepository', 'Aspire\\Foundation\\Repositories\\Eloquent\\LoanRepository');
        $this->app->bind('Aspire\\Foundation\\Repositories\\Contracts\\LoanRepaymentRepository', 'Aspire\\Foundation\\Repositories\\Eloquent\\LoanRepaymentRepository');
    }

    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'AspireFoundation');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
    }
}
