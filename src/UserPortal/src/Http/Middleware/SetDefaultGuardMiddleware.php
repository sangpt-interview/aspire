<?php
declare(strict_types=1);

namespace Aspire\UserPortal\Http\Middleware;


use Closure;

/**
 * Class SetDefaultGuardMiddleware
 * @package Aspire\UserPortal\Http\Middleware
 */
class SetDefaultGuardMiddleware
{
    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        config([
            'auth.defaults.guard' => 'apiUser'
        ]);

        return $next($request);
    }
}
