<?php
declare(strict_types=1);

namespace Aspire\UserPortal\Http\Controllers\API\V1;


use Aspire\Foundation\Http\Controllers\Controller as AspireController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class LogoutController
 * @package Aspire\UserPortal\Http\Controllers\API\V1
 */
class LogoutController extends AspireController
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(Request $request): JsonResponse
    {
        Auth::logout();
        return $this->success(__('AspireFoundation::messages.success'));
    }
}
