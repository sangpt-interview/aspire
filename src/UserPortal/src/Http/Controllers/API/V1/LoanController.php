<?php
declare(strict_types=1);

namespace Aspire\UserPortal\Http\Controllers\API\V1;


use Aspire\Foundation\Http\Controllers\Controller as AspireController;
use Aspire\Foundation\Models\Loan;
use Aspire\Foundation\Services\CreateLoanService;
use Aspire\Foundation\Services\GetLoanDetailService;
use Aspire\Foundation\Services\GetLoanListService;
use Aspire\Foundation\Services\Requests\CreateLoanRequest as CreateLoanRequestFoundation;
use Aspire\Foundation\Services\Requests\GetLoanListRequest;
use Aspire\Foundation\Services\Requests\UpdateLoanRequest as UpdateLoanRequestFoundation;
use Aspire\UserPortal\Http\Requests\CreateLoanRequest;
use Aspire\UserPortal\Http\Requests\ShowLoanRequest;
use Aspire\UserPortal\Http\Requests\UpdateLoanRequest;
use Aspire\UserPortal\Services\UpdateLoanService;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

/**
 * Class LoanController
 * @package Aspire\UserPortal\Http\Controllers\API\V1
 */
class LoanController extends AspireController
{
    /**
     * @var GetLoanListService
     */
    protected GetLoanListService $getLoanListService;
    /**
     * @var GetLoanDetailService
     */
    protected GetLoanDetailService $getLoanDetailService;
    /**
     * @var CreateLoanService
     */
    protected CreateLoanService $createLoanService;
    /**
     * @var UpdateLoanService
     */
    protected UpdateLoanService $updateLoanService;

    /**
     * LoanController constructor.
     * @param GetLoanListService $getLoanListService
     * @param GetLoanDetailService $getLoanDetailService
     * @param CreateLoanService $createLoanService
     * @param UpdateLoanService $updateLoanService
     */
    public function __construct(
        GetLoanListService $getLoanListService,
        GetLoanDetailService $getLoanDetailService,
        CreateLoanService $createLoanService,
        UpdateLoanService $updateLoanService
    ) {
        $this->getLoanListService = $getLoanListService;
        $this->getLoanDetailService = $getLoanDetailService;
        $this->createLoanService = $createLoanService;
        $this->updateLoanService = $updateLoanService;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $result = $this->getLoanListService->execute(
            new GetLoanListRequest(Auth::id())
        );

        return $this->success(
            __('AspireFoundation::messages.success'),
            $result
        );
    }

    /**
     * @param ShowLoanRequest $request
     * @param Loan $loan
     * @return JsonResponse
     */
    public function show(ShowLoanRequest $request, Loan $loan): JsonResponse
    {
        $result = $this->getLoanDetailService->execute((int) $loan->id);

        return $this->success(
            __('AspireFoundation::messages.success'),
            $result
        );
    }

    /**
     * @param CreateLoanRequest $request
     * @return JsonResponse
     */
    public function store(CreateLoanRequest $request): JsonResponse
    {
        $this->createLoanService->execute(
            new CreateLoanRequestFoundation(
                (int) Auth::id(),
                (float) $request->get('amount_required'),
                Carbon::createFromFormat('Y-m-d', $request->get('loan_term'))
            )
        );

        return $this->success(__('AspireFoundation::messages.success'));
    }

    /**
     * @param UpdateLoanRequest $request
     * @param Loan $loan
     * @return JsonResponse
     */
    public function update(UpdateLoanRequest $request, Loan $loan): JsonResponse
    {
        $this->updateLoanService->execute(
            new UpdateLoanRequestFoundation(
                $loan,
                (int) Auth::id(),
                (float) $request->get('amount_required'),
                Carbon::createFromFormat('Y-m-d', $request->get('loan_term'))
            )
        );

        return $this->success(__('AspireFoundation::messages.success'));
    }
}