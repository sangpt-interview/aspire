<?php
declare(strict_types=1);

namespace Aspire\UserPortal\Http\Controllers\API\V1;


use Aspire\Foundation\Http\Controllers\Controller as AspireController;
use Aspire\Foundation\Models\Loan;
use Aspire\UserPortal\Http\Requests\RepayRequest as RepayFormRequest;
use Aspire\UserPortal\Services\RepayRequest;
use Aspire\UserPortal\Services\RepayService;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class RepayController
 * @package Aspire\UserPortal\Http\Controllers\API\V1
 */
class RepayController extends AspireController
{
    /**
     * @param RepayService $repayService
     * @param RepayFormRequest $repayFormRequest
     * @param Loan $loan
     * @return JsonResponse
     */
    public function __invoke(RepayService $repayService, RepayFormRequest $repayFormRequest, Loan $loan): JsonResponse
    {
        $repayService->execute(
            new RepayRequest(
                $loan,
                (float) $repayFormRequest->get('amount_repaid'),
                Carbon::now()
            )
        );

        return $this->success(__('AspireFoundation::messages.success'));
    }
}