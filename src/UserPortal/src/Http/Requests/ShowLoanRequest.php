<?php
declare(strict_types=1);

namespace Aspire\UserPortal\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Class ShowLoanRequest
 * @package Aspire\UserPortal\Http\Requests
 */
class ShowLoanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $loan = $this->route('loan');
        $user = Auth::user();

        return (int) $loan->user_id === (int) $user->id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}