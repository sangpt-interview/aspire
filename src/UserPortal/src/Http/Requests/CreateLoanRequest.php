<?php
declare(strict_types=1);

namespace Aspire\UserPortal\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateLoanRequest
 * @package Aspire\UserPortal\Http\Requests
 */
class CreateLoanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount_required' => [
                'required',
                'numeric',
            ],
            'loan_term' => [
                'required',
                'date_format:Y-m-d',
                'after:today'
            ],
        ];
    }
}