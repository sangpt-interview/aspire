<?php
declare(strict_types=1);

namespace Aspire\UserPortal;


use Aspire\Foundation\Models\User;
use Aspire\UserPortal\ServiceProviders\UserPortalRouteServiceProvider;
use Illuminate\Support\ServiceProvider;

/**
 * Class UserPortalServiceProvider
 * @package Aspire\UserPortal\ServiceProviders
 */
class UserPortalServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->configAuth();

        $this->app->register(UserPortalRouteServiceProvider::class);
    }

    protected function configAuth()
    {
        config([
            'auth.providers.users' => [
                'driver' => 'eloquent',
                'model' => User::class
            ],
            'auth.guards.apiUser' => [
                'driver' => 'jwt',
                'provider' => 'users',
            ]
        ]);
    }
}
