<?php
declare(strict_types=1);

namespace Aspire\UserPortal\Services;


use Aspire\Foundation\Models\Loan;
use Carbon\Carbon;

/**
 * Class RepayRequest
 * @package Aspire\UserPortal\Services
 */
class RepayRequest
{
    /**
     * @var Loan
     */
    protected Loan $loan;
    /**
     * @var float
     */
    protected float $amountRepaid;
    /**
     * @var Carbon
     */
    protected Carbon $timeRepaid;

    /**
     * SubmitRepaymentRequest constructor.
     * @param Loan $loan
     * @param float $amountRepaid
     * @param Carbon $timeRepaid
     */
    public function __construct(Loan $loan, float $amountRepaid, Carbon $timeRepaid)
    {
        $this->loan = $loan;
        $this->amountRepaid = $amountRepaid;
        $this->timeRepaid = $timeRepaid;
    }

    /**
     * @return Loan
     */
    public function getLoan(): Loan
    {
        return $this->loan;
    }

    /**
     * @return float
     */
    public function getAmountRepaid(): float
    {
        return $this->amountRepaid;
    }

    /**
     * @return Carbon
     */
    public function getTimeRepaid(): Carbon
    {
        return $this->timeRepaid;
    }
}