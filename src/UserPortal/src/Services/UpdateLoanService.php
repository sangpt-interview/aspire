<?php
declare(strict_types=1);

namespace Aspire\UserPortal\Services;


use Aspire\Foundation\Enums\LoanStatusEnum;
use Aspire\Foundation\Services\Requests\UpdateLoanRequest;
use Aspire\Foundation\Services\UpdateLoanService as UpdateLoanServiceFoundation;

/**
 * Class UpdateLoanService
 * @package Aspire\UserPortal\Services
 */
class UpdateLoanService extends UpdateLoanServiceFoundation
{
    /**
     * @param UpdateLoanRequest $updateLoanRequest
     */
    public function execute(UpdateLoanRequest $updateLoanRequest)
    {
        if ($this->isLoanValidForUpdate($updateLoanRequest)) {
            parent::execute($updateLoanRequest);
        }
    }

    /**
     * @param UpdateLoanRequest $updateLoanRequest
     * @return bool
     */
    protected function isLoanValidForUpdate(UpdateLoanRequest $updateLoanRequest): bool
    {
        return $updateLoanRequest->getLoan()->status === LoanStatusEnum::NEW;
    }
}