<?php
declare(strict_types=1);

namespace Aspire\UserPortal\Services;


use Aspire\Foundation\Enums\LoanStatusEnum;
use Aspire\Foundation\Repositories\Contracts\LoanRepaymentRepository;
use Aspire\Foundation\Repositories\Contracts\LoanRepository;

/**
 * Class RepayService
 * @package Aspire\UserPortal\Services
 */
class RepayService
{
    /**
     * @var LoanRepaymentRepository
     */
    protected LoanRepaymentRepository $loanRepaymentRepository;
    /**
     * @var LoanRepository
     */
    protected LoanRepository $loanRepository;

    /**
     * RepayService constructor.
     * @param LoanRepaymentRepository $loanRepaymentRepository
     * @param LoanRepository $loanRepository
     */
    public function __construct(LoanRepaymentRepository $loanRepaymentRepository, LoanRepository $loanRepository)
    {
        $this->loanRepaymentRepository = $loanRepaymentRepository;
        $this->loanRepository = $loanRepository;
    }

    /**
     * @param RepayRequest $request
     */
    public function execute(RepayRequest $request)
    {
        if (! $this->isLoanValidForRepay($request)) {
            throw new \LogicException(__("Only loans on status approved could be repaid"), 400);
        }

        if (! $this->isAmountRepaidValid($request)) {
            throw new \LogicException(__("Repaid amount is over than {$request->getLoan()->remaining_amount}"), 400);
        }

        $this->loanRepaymentRepository->create([
            'loan_id' => (int) $request->getLoan()->id,
            'amount_repaid' => (float) $request->getAmountRepaid(),
            'time_repaid' => $request->getTimeRepaid()
        ]);

        $this->loanRepository->repaid((int) $request->getLoan()->id, $request->getAmountRepaid());

        if ($this->haveFinishedLoan($request)) {
            $this->loanRepository->finishLoan((int) $request->getLoan()->id);
        }
    }

    /**
     * @param RepayRequest $request
     * @return bool
     */
    protected function isLoanValidForRepay(RepayRequest $request): bool
    {
        return $request->getLoan()->status === LoanStatusEnum::APPROVED;
    }

    /**
     * @param RepayRequest $request
     * @return bool
     */
    protected function isAmountRepaidValid(RepayRequest $request): bool
    {
        return $request->getAmountRepaid() <= (float) $request->getLoan()->remaining_amount;
    }

    /**
     * @param RepayRequest $request
     * @return bool
     */
    protected function haveFinishedLoan(RepayRequest $request): bool
    {
        return $request->getAmountRepaid() === (float) $request->getLoan()->remaining_amount;
    }
}