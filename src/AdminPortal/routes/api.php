<?php
declare(strict_types=1);

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'LoginController');

Route::group([
    'middleware' => 'auth:apiAdmin',
], function () {
    Route::post('logout', 'LogoutController');

    Route::apiResource('loan', 'LoanController')->only(['index', 'show']);

    Route::patch('loan/{loan}/approve', 'ApproveLoanController');
});
