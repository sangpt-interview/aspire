<?php
declare(strict_types=1);

namespace Aspire\AdminPortal\ServiceProviders;


use Aspire\AdminPortal\Http\Middleware\SetDefaultGuardMiddleware;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

/**
 * Class AdminPortalRouteServiceProvider
 * @package Aspire\AdminPortal\ServiceProviders
 */
class AdminPortalRouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Aspire\AdminPortal\Http\Controllers';

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        Route::prefix('api/v1/admin')
            ->middleware(['api', SetDefaultGuardMiddleware::class])
            ->namespace($this->namespace . '\API\V1')
            ->group(__DIR__ . '/../../routes/api.php');
    }
}
