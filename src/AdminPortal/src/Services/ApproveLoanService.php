<?php
declare(strict_types=1);

namespace Aspire\AdminPortal\Services;


use Aspire\Foundation\Enums\LoanStatusEnum;
use Aspire\Foundation\Models\Loan;
use Aspire\Foundation\Repositories\Contracts\LoanRepository;

/**
 * Class ApproveLoanService
 * @package Aspire\AdminPortal\Services
 */
class ApproveLoanService
{
    /**
     * @var LoanRepository
     */
    protected LoanRepository $loanRepository;

    /**
     * ApproveLoanService constructor.
     * @param LoanRepository $loanRepository
     */
    public function __construct(LoanRepository $loanRepository)
    {
        $this->loanRepository = $loanRepository;
    }

    /**
     * @param Loan $loan
     * @param int $approvedBy
     */
    public function execute(Loan $loan, int $approvedBy)
    {
        if (! $this->isLoanValidForApprove($loan)) {
            throw new \LogicException(__('Only loans in status new could be approved'), 400);
        }

        $this->loanRepository->approve((int) $loan->id, $approvedBy);
    }

    /**
     * @param Loan $loan
     * @return bool
     */
    protected function isLoanValidForApprove(Loan $loan): bool
    {
        return (string) $loan->status === LoanStatusEnum::NEW;
    }
}