<?php
declare(strict_types=1);

namespace Aspire\AdminPortal\Http\Controllers\API\V1;


use Aspire\Foundation\Http\Controllers\Controller as AspireController;
use Aspire\Foundation\Models\Loan;
use Aspire\Foundation\Services\GetLoanDetailService;
use Aspire\Foundation\Services\GetLoanListService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class LoanController
 * @package Aspire\AdminPortal\Http\Controllers\API\V1
 */
class LoanController extends AspireController
{
    /**
     * @var GetLoanListService
     */
    protected GetLoanListService $getLoanListService;
    /**
     * @var GetLoanDetailService
     */
    protected GetLoanDetailService $getLoanDetailService;

    /**
     * LoanController constructor.
     * @param GetLoanListService $getLoanListService
     * @param GetLoanDetailService $getLoanDetailService
     */
    public function __construct(
        GetLoanListService $getLoanListService,
        GetLoanDetailService $getLoanDetailService
    ) {
        $this->getLoanListService = $getLoanListService;
        $this->getLoanDetailService = $getLoanDetailService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $result = $this->getLoanListService->execute();

        return $this->success(
            __('AspireFoundation::messages.success'),
            $result
        );
    }

    /**
     * @param Loan $loan
     * @return JsonResponse
     */
    public function show(Loan $loan): JsonResponse
    {
        $result = $this->getLoanDetailService->execute((int) $loan->id);

        return $this->success(
            __('AspireFoundation::messages.success'),
            $result
        );
    }
}