<?php
declare(strict_types=1);

namespace Aspire\AdminPortal\Http\Controllers\API\V1;


use Aspire\AdminPortal\Services\ApproveLoanService;
use Aspire\Foundation\Http\Controllers\Controller as AspireController;
use Aspire\Foundation\Models\Loan;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

/**
 * Class ApproveLoanController
 * @package Aspire\AdminPortal\Http\Controllers\API\V1
 */
class ApproveLoanController extends AspireController
{
    /**
     * @param ApproveLoanService $approveLoanService
     * @param Loan $loan
     * @return JsonResponse
     */
    public function __invoke(ApproveLoanService $approveLoanService, Loan $loan): JsonResponse
    {
        $approveLoanService->execute($loan, (int) Auth::id());
        return $this->success(__('AspireFoundation::messages.success'));
    }
}