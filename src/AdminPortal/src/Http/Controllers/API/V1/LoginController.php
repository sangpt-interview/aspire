<?php
declare(strict_types=1);

namespace Aspire\AdminPortal\Http\Controllers\API\V1;


use Aspire\Foundation\Http\Controllers\Controller as AspireController;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class LoginController
 * @package Aspire\AdminPortal\Http\Controllers\API\V1
 */
class LoginController extends AspireController
{
    /**
     * @param Request $request
     * @return JsonResponse
     * @throws AuthenticationException
     */
    public function __invoke(Request $request): JsonResponse
    {
        $request->validate([
            'email' => ['required'],
            'password' => ['required']
        ]);

        try {
            if ($token = Auth::attempt($request->only('email', 'password'))) {
                return $this->success(__('AspireFoundation::messages.success'), [
                    'token' => $token
                ]);
            }
        } catch (\Throwable $e) {}

        throw new AuthenticationException();
    }
}