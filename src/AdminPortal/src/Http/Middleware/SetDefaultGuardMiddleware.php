<?php
declare(strict_types=1);

namespace Aspire\AdminPortal\Http\Middleware;


use Closure;

/**
 * Class SetDefaultGuardMiddleware
 * @package Aspire\AdminPortal\Http\Middleware
 */
class SetDefaultGuardMiddleware
{
    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        config([
            'auth.defaults.guard' => 'apiAdmin'
        ]);

        return $next($request);
    }
}
