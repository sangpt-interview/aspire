<?php
declare(strict_types=1);

namespace Aspire\AdminPortal;


use Aspire\AdminPortal\ServiceProviders\AdminPortalRouteServiceProvider;
use Aspire\Foundation\Models\Admin;
use Illuminate\Support\ServiceProvider;

/**
 * Class AdminPortalServiceProvider
 * @package Aspire\AdminPortal
 */
class AdminPortalServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->configAuth();

        $this->app->register(AdminPortalRouteServiceProvider::class);
    }

    protected function configAuth()
    {
        config([
            'auth.providers.admins' => [
                'driver' => 'eloquent',
                'model' => Admin::class
            ],
            'auth.guards.apiAdmin' => [
                'driver' => 'jwt',
                'provider' => 'admins',
            ],
        ]);
    }
}
