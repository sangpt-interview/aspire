<?php
declare(strict_types=1);

namespace Tests\Unit\src\UserPortal\src\Services;


use Aspire\Foundation\Models\Loan;
use Aspire\Foundation\Repositories\Contracts\LoanRepaymentRepository;
use Aspire\Foundation\Repositories\Contracts\LoanRepository;
use Aspire\UserPortal\Services\RepayRequest;
use Aspire\UserPortal\Services\RepayService;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use Mockery as m;

/**
 * Class RepayServiceTest
 * @package Tests\Unit\src\UserPortal\src\Services
 */
class RepayServiceTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_repay_when_amount_repaid_over_than_remaining_amount()
    {
        $loanRepository = m::mock(LoanRepository::class)->makePartial();
        $loanRepaymentRepository = m::mock(LoanRepaymentRepository::class)->makePartial();
        $this->repayService = new RepayService($loanRepaymentRepository, $loanRepository);

        $loanRepaymentRepository->shouldReceive('create');
        $loanRepository->shouldReceive('repaid');

        $repayRequest = m::mock(RepayRequest::class)->makePartial();
        $loan = m::mock(Loan::class)->makePartial();
        $repayRequest->shouldReceive('getLoan')->andReturn($loan);
        $repayRequest->shouldReceive('getAmountRepaid')->andReturn(3000);
        $repayRequest->shouldReceive('getTimeRepaid')->andReturn(Carbon::now());

        $loan->shouldReceive('getAttribute')->with('remaining_amount')->andReturn(2000);
        $loan->shouldReceive('getAttribute')->with('status')->andReturn('approved');

        try {
            $this->repayService->execute($repayRequest);
        } catch (\Throwable $e) {
            $this->assertInstanceOf(\LogicException::class, $e);
            $this->assertEquals("Repaid amount is over than 2000", $e->getMessage());
        }
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_repay_when_loan_is_new()
    {
        $loanRepository = m::mock(LoanRepository::class)->makePartial();
        $loanRepaymentRepository = m::mock(LoanRepaymentRepository::class)->makePartial();
        $this->repayService = new RepayService($loanRepaymentRepository, $loanRepository);

        $loanRepaymentRepository->shouldReceive('create');
        $loanRepository->shouldReceive('repaid');

        $repayRequest = m::mock(RepayRequest::class)->makePartial();
        $loan = m::mock(Loan::class)->makePartial();
        $repayRequest->shouldReceive('getLoan')->andReturn($loan);
        $repayRequest->shouldReceive('getAmountRepaid')->andReturn(1000);
        $repayRequest->shouldReceive('getTimeRepaid')->andReturn(Carbon::now());

        $loan->shouldReceive('getAttribute')->with('remaining_amount')->andReturn(2000);
        $loan->shouldReceive('getAttribute')->with('status')->andReturn('new');

        try {
            $this->repayService->execute($repayRequest);
        } catch (\Throwable $e) {
            $this->assertInstanceOf(\LogicException::class, $e);
            $this->assertEquals('Only loans on status approved could be repaid', $e->getMessage());
        }
    }
}
