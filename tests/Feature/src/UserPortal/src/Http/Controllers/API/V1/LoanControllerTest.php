<?php
declare(strict_types=1);

namespace Tests\Feature\src\UserPortal\src\Http\Controllers\API\V1;

use Aspire\Foundation\Models\Loan;
use Aspire\Foundation\Models\User;
use Aspire\Foundation\Services\CreateLoanService;
use Aspire\Foundation\Services\GetLoanDetailService;
use Aspire\Foundation\Services\GetLoanListService;
use Aspire\UserPortal\Http\Controllers\API\V1\LoanController;
use Aspire\UserPortal\Http\Requests\CreateLoanRequest;
use Aspire\UserPortal\Http\Requests\ShowLoanRequest;
use Aspire\UserPortal\Http\Requests\UpdateLoanRequest;
use Aspire\UserPortal\Services\UpdateLoanService;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Tests\TestCase;
use Mockery as m;

/**
 * Class LoanControllerTest
 * @package Tests\Feature\src\UserPortal\src\Http\Controllers\API\V1
 */
class LoanControllerTest extends TestCase
{
    /**
     * @throws BindingResolutionException
     */
    protected function setUp(): void
    {
        $this->afterApplicationCreated(function () {
            $this->loanController = new LoanController(
                $this->app->make(GetLoanListService::class),
                $this->app->make(GetLoanDetailService::class),
                $this->app->make(CreateLoanService::class),
                $this->app->make(UpdateLoanService::class)
            );

            $user = m::mock(User::class)->makePartial();
            $user->shouldReceive('getAttribute')->with('id')->andReturn(1);
            $this->actingAs($user, 'apiUser');
        });

        parent::setUp();
    }

    public function test_it_can_get_list()
    {
        $res = $this->loanController->index();
        $this->assertInstanceOf(JsonResponse::class, $res);
    }

    public function test_it_can_show()
    {
        $showLoanRequest = m::mock(ShowLoanRequest::class)->makePartial();
        $loan = m::mock(Loan::class)->makePartial();
        $showLoanRequest->shouldReceive('route')->andReturn($loan);
        $loan->shouldReceive('getAttribute')->with('id')->andReturn(1);
        $loan->shouldReceive('getAttribute')->with('user_id')->andReturn(1);

        $res = $this->loanController->show($showLoanRequest, $loan);
        $this->assertInstanceOf(JsonResponse::class, $res);
    }

    public function test_it_can_store()
    {
        $createLoanRequest = m::mock(CreateLoanRequest::class)->makePartial();
        $createLoanRequest
            ->shouldReceive('get')
            ->once()
            ->with('amount_required')
            ->andReturn('5000');
        $createLoanRequest
            ->shouldReceive('get')
            ->once()
            ->with('loan_term')
            ->andReturn('2021-11-30');

        $res = $this->loanController->store($createLoanRequest);
        $this->assertInstanceOf(JsonResponse::class, $res);
    }

    public function test_it_can_update()
    {
        $updateLoanRequest = m::mock(UpdateLoanRequest::class)->makePartial();
        $loan = m::mock(Loan::class)->makePartial();
        $updateLoanRequest->shouldReceive('route')->andReturn($loan);
        $loan->shouldReceive('getAttribute')->with('id')->andReturn(1);
        $loan->shouldReceive('getAttribute')->with('user_id')->andReturn(1);
        $loan->shouldReceive('getAttribute')->with('status')->andReturn('new');

        $updateLoanRequest
            ->shouldReceive('get')
            ->once()
            ->with('amount_required')
            ->andReturn('5005');
        $updateLoanRequest
            ->shouldReceive('get')
            ->once()
            ->with('loan_term')
            ->andReturn('2021-12-03');

        $res = $this->loanController->update($updateLoanRequest, $loan);
        $this->assertInstanceOf(JsonResponse::class, $res);
    }
}
